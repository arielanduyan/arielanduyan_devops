#!/bin/bash

mvn sonar:sonar \
-Dsonar.projectKey=$SONAR_KEY \
-Dsonar.projectName=$SONAR_KEY \
-Dsonar.projectVersion=$APP_VERSION \
-Dsonar.host.url=$SONAR_URL \
-Dsonar.login=$SONAR_TOKEN \
-Dsonar.sources=src/main/ \
-Dsonar.inclusions=**/*.java \
-Dsonar.junit.reportPaths=target/surefire-reports \
-Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco/jacoco.xml
