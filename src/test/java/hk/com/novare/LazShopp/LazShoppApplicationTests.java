package hk.com.novare.LazShopp;

import hk.com.novare.LazShopp.entity.Product;
import hk.com.novare.LazShopp.repository.ProductRepository;
import hk.com.novare.LazShopp.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class LazShoppApplicationTests {

	@InjectMocks
	ProductServiceImpl ps;

	@Mock
	ProductRepository productRepository;

	@Test
	void contextLoads() {
	}

	@Test
	@DisplayName("this should return all lists of products")
	public void shouldReturnAllProducts() {
		List<Product> expected = List.of(new Product());
		Mockito.when(productRepository.findAll()).thenReturn(expected);

		List<Product> actual = ps.getAllProducts();
		Assertions.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("this should return new added products")
	public void shouldReturnNewProducts() {
		Product expected = new Product();
		Mockito.when(productRepository.save(expected)).thenReturn(expected);

		Product actual = ps.addNewProduct(expected);
		Assertions.assertEquals(expected, actual);
	}

	@Test
	@DisplayName("this should return throw an exception for updateProduct")
	public void shouldReturnException() throws Exception {
		Product expected = new Product();
		//Mockito.when(productRepository.getOne(id)).thenReturn(expected);
		Assertions.assertThrows(Exception.class, () -> { ps.updateProduct(expected); });
	}

	//@Test
	//@DisplayName("this should return sell products")
	//public void shouldReturnSellProducts() {
	//	int id = 1;
	//	int quantity = 100;
	//	Product expected = new Product();

	//	Product actual = ps.sellProduct(id, quantity);
	//	Assertions.assertEquals((expected.getId(), expected.getQuantity()), actual);
	//}

	@Test
	@DisplayName("this should return throw NullPointerException for sellProduct")
	public void shouldReturnNullException() throws NullPointerException {
		Product expected = new Product();
		//Mockito.when(productRepository.getOne(id)).thenReturn(expected);
		Assertions.assertThrows(NullPointerException.class, () -> { ps.sellProduct(0, 100); });
	}

}
